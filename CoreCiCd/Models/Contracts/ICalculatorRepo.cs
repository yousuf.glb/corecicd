﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreCiCd.Models.Contracts
{
    public interface ICalculatorRepo
    {
        int add(int a, int b);
    }
}
