﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreCiCd.Models.Contracts;

namespace CoreCiCd.Models.Mocks
{
    public class CalculatorRepoMock : ICalculatorRepo
    {
        public int add(int a, int b)
        {
            int result = a + b;
            if(result > 10)
            {
                throw new ArithmeticException("Please check the values");
            }
            else
            {
                return a + b;
            }
           
        }
    }
}
