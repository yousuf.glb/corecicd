using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using CoreCiCd.Models.Mocks;

namespace XUnitTestProject
{
    public class CalculatorUnitTest
    {
        [Fact]
        public void ValidateAdd()
        {
            CalculatorRepoMock Obj = new CalculatorRepoMock();
            int result = Obj.add(4,4);

            Assert.Equal(8,result);
        }

        [Fact]
        public void InvalidateAdd()
        {
            CalculatorRepoMock Obj = new CalculatorRepoMock();
            Assert.Throws<ArithmeticException>(() => Obj.add(5, 7));
        }        
    }
}
